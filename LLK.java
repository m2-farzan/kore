package kore;

import java.util.Arrays;
import static kore.ByteTools.*;
import java.io.*;
import java.net.*;

public class LLK
{
	private Socket socket;
	private BufferedInputStream input;
	private BufferedOutputStream output;

	private HLK hlk; // for debug logs //todo: create a better logging mech'sm
	
	public LLK(HLK _hlk)
	{
		hlk = _hlk;
	}
	
	private byte[] unixTimeId()
	{
		long unixTimeX = unixTimeTE32();
		unixTimeX-=(unixTimeX%4);
		return littleEndian(unixTimeX, 8);
	}
	private long timeCorrectionParam=0;
	private long unixTimeTE32()
	{
		final long MAX_TIME_ERROR = 429496729L; // 100 milli
		long relativeTime = (long)(System.nanoTime() * 4.294967296); //4.29 = 2^32 dev 10^9
		long conventionTime = relativeTime + timeCorrectionParam;
		long realTime = (long)(System.currentTimeMillis() * 4294967.296);
		long diff = Math.abs(conventionTime - realTime);
		if(diff > MAX_TIME_ERROR)
		{
			timeCorrectionParam = realTime - relativeTime;
			return unixTimeTE32();
		}
		return conventionTime;
	}
	
	
	private Bytes applyUnencryptedHeaders(Bytes data)
	{
		Bytes message = new Bytes();
		
		byte[] authKeyId = new byte[8];
		Arrays.fill(authKeyId, (byte)0);
		message.add(authKeyId);
		
		byte[] messageId = unixTimeId();
		message.add(unixTimeId());
		
		int length = data.size();
		byte[] messageLength = littleEndian(length, 4);
		message.add(messageLength);
		
		message.addAll(data);
		
		return message;
	}
	private void appendTotalLengthHeader(Bytes message)
	{
		Bytes header = new Bytes();
		int length = message.size();
		
		if(length/4 < 256)
		{
			header.add((byte) 0xEF);
			header.add((byte)(length/4));
		}
		
		message.addAll(0,header);
		
	}
	private void removeTotalLengthHeader(Bytes message) throws InvalidObjectException
	{
		if(message.get(0)<0x7f)
		{
			int length = message.get(0)*4;
			if(message.size() != length + 1)
			{
				throw new InvalidObjectException("Message length does not match header.");
			}
			message.remove(0);
		}
		else if(message.get(0)==0x7f)
		{
			int length = 4 * inverseLittleEndian(message.subSet(1, 4));
			if(message.size() != length + 4)
			{
				throw new InvalidObjectException("Message length does not match header.");
			}
			message.subList(0, 4).clear();
		}
	}
	private void removeUnencryptedHeaders(Bytes message)
	{
		if(message.size()<20)
			return;
		message.subList(0, 20).clear();
	}
	
	
	private void openPorts() throws IOException
	{
		final int SOCKET_TIMEOUT = 16000; //todo: appropriate amount
		final String SERVER_IP = "149.154.167.40";
		final int SERVER_PORT = 443;
		socket = new Socket(SERVER_IP, SERVER_PORT);
		socket.setSoTimeout(SOCKET_TIMEOUT);
		output = new BufferedOutputStream(socket.getOutputStream());
		input = new BufferedInputStream(socket.getInputStream());
	}
	private void closePorts() throws IOException
	{
		//todo: option to keep ports open
		input.close();
		output.close();
		socket.close();
	}
	public void closeAllPorts()
	{
		try
		{
			input.close();
			output.close();
			socket.close();
		}
		catch (IOException e)
		{
			//failed.
		}
	}
	private Bytes sendTcp(Bytes payload) throws IOException
	{
//		printLog(LINE_SEND, payload);

		int BUFFER_SIZE = 256;
		
		byte[] outMsg = payload.toPbytes();
		output.write(outMsg,0,outMsg.length);
		output.flush();
		
		Bytes answer = new Bytes();
		byte[] inFragment = new byte[BUFFER_SIZE];
		int z = BUFFER_SIZE;
		while(z==BUFFER_SIZE)
		{
			z=input.read(inFragment);
			answer.add(inFragment);
		}
		answer.cutEnd(BUFFER_SIZE - z);

//		printLog(LINE_REC, answer);

		return answer;
	}

	public Bytes sendUnencryptedMessage(Bytes data) throws IOException
	{
		Bytes message = applyUnencryptedHeaders(data);
		appendTotalLengthHeader(message);
		
		Bytes response;
		openPorts();
		response = sendTcp(message);
		closePorts();
		
		removeTotalLengthHeader(response);
		removeUnencryptedHeaders(response);
		
		return response;
	}

	private final boolean LINE_LOG = true;
	private final int LINE_SEND = 1;
	private final int LINE_REC = 0;
	private void printLog(int CAP, byte[] bytes)
	{
		if(!LINE_LOG)
			return;
		String cap = CAP==LINE_SEND ? "SENT" : "RECEIVED";
		String r = cap + " " + bytes.length + " BYTES:\n{\n";
		r += toHex(bytes);
		if(bytes.length%8!=0)
			r += "\n";
		r += "}\n";
		hlk.log(r);
	}
	private void printLog(int CAP, Bytes bytes)
	{
		printLog(CAP, bytes.toPbytes());
	}
}
