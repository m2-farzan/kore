package kore;

import java.util.ArrayList;
import java.util.Arrays;

public class Bytes extends ArrayList<Byte>
{
	public Bytes()
	{
		super();
	}
	
	public Bytes(byte[] bytes)
	{
		super();
		this.add(bytes);
	}
	
	public void add(Byte[] bytes)
	{
		this.addAll(Arrays.asList(bytes));
	}
	
	public void add(byte[] bytes)
	{
		int L = bytes.length;
		Byte[] t = new Byte[L];
		for(int i=0;i<L;i++)
			t[i] = bytes[i];
		this.add(t);
	}
	
	public void add(byte b)
	{
		this.add(Byte.valueOf(b));
	}
	
	public byte[] toPbytes()
	{
		int length = this.size();
		byte[] r = new byte[length];
		for(int i=0;i<length;i++)
			r[i]=this.get(i);
		return r;
	}
	
	public void cutEnd(int n)
	{
		int length = this.size();
		for(int i=0;i<n;i++)
			this.remove(length-1-i);
	}
	
	public byte[] subSet(int start, int end)
	{
		int c = end - start;
		byte[] r = new byte[c];
		for(int i=0;i<c;i++)
			r[i] = this.get(start+i);
		return r;
	}
	
	public static Bytes fromPbytes(byte[] bytes)
	{
		Bytes r = new Bytes();
		r.add(bytes);
		return r;
	}
	
	public static Bytes fromHexString (String hex)
	{
		Bytes r = new Bytes();
		for(int i=0;i<hex.length();i+=2)
		{
			String s = hex.substring(i,i+2);
			r.add((byte)Integer.parseInt(s,16));
		}
		return r;
	}
	
	@Override
	public String toString()
	{
		if(this.size()==0)
		{
			return ("\n[Empty Bytes]");
		}
		final int colSize = 8;
		int length = this.size();
		String r = "";
		for(int i = 0; i < length/colSize + 1; i++)
		{
			if(i!=0)
				r+="\n";
			for(int j=0;j<colSize;j++)
			{
				if(i*colSize+j >= length)
					break;
				String p = String.format("%02X ", this.get(i*colSize+j));
				r+=(p+" ");
			}
		}
		return r;
	}
}
