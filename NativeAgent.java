package kore;

public class NativeAgent
{
	static
	{
		System.loadLibrary("mathematician");
	}
	private static native long SEMIPRIMEFACTOR(byte[] data);
	public static long semiprimeFactor(byte[] data)
	{
		return SEMIPRIMEFACTOR(data);
	}
}