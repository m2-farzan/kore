package kore;

/**
 * Created by mostafa on 5/30/17.
 */
public interface ResponseInterface
{
    void log(String text);

    void postMessage(int arg1);

    void postMessage(int arg1, int arg2);

    void postMessage(int arg1, int arg2, Object obj);

    void postError(int errorCode);
}