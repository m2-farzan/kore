package kore;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.net.SocketTimeoutException;

import static kore.ByteTools.*;


public class MLK
{
	//<editor-fold desc="Schema Constants">
	private static class SCHEMA
	{
		public static final byte[] REQ_PQ = schemaBytes(0x60469778);
		public static final byte[] RES_PQ = schemaBytes(0x05162463);
		public static final byte[] P_Q_INNER_DATA = schemaBytes(0x83c95aec);
		public static final byte[] REQ_DH_PARAMS = schemaBytes(0xd712e4be);
		public static final byte[] SERVER_DH_PARAMS_OK = schemaBytes(0xd0e8075c);
		public static final byte[] SERVER_DH_INNER_DATA = schemaBytes(0xb5890dba);
		public static final byte[] SET_CLIENT_DH_PARAMS = schemaBytes(0xf5045f1f);
		public static final byte[] CLIENT_DH_INNER_DATA = schemaBytes(0x6643b654);
		public static final byte[] DH_GEN_OK = schemaBytes(0x3bcbf734);

		public static final byte[] VECTOR_LONG = schemaBytes(0x1cb5c415);
	}
	//</editor-fold>
	
	
	private HLK hlk; //used for IntCodes & Debug Logs
	private LLK llk;
	private ICLK iclk;
	
	
	public MLK(HLK _hlk)
	{
		hlk = _hlk;
		setupLLK();
	}
	private void setupLLK()
	{
		if(llk == null)
			llk = new LLK(hlk);
	}
	public void closeAllPorts()
	{
		llk.closeAllPorts();
	}

	public int createAuthKey()
	{
		iclk = new ICLK();
		Bytes response = new Bytes();
		int returnCode;
		if((returnCode = createAuthKey_SendMsg1(response))<0)
			return returnCode;
		if((returnCode = createAuthKey_DigestMsg1(response))<0)
			return returnCode;
		if((returnCode = createAuthKey_SendMsg2(response))<0)
			return returnCode;
		if((returnCode = createAuthKey_DigestMsg2(response))<0)
			return returnCode;
		if((returnCode = createAuthKey_SendMsg3(response))<0)
			return returnCode;
		if((returnCode = createAuthKey_DigestMsg3(response))<0)
			return returnCode;
		return 0; //todo:eire
	}
	private int createAuthKey_SendMsg1(Bytes response)
	{
		response.clear();
		//*** req_pq#60469778 nonce:int128 = ResPQ
		OTLStream msg = new OTLStream();

		iclk.genNonce();

		msg.appConr(SCHEMA.REQ_PQ);
		msg.appInt128(iclk.nonce);
		
		try
		{
			response.addAll(llk.sendUnencryptedMessage(msg.getBytes()));
		}
		catch (IOException e)
		{
			if(e instanceof SocketTimeoutException)
				return HLK.STATUS_CODE.TIMEOUT;
			if(e instanceof InvalidObjectException)
				return HLK.STATUS_CODE.HEADER_MISMATCH; //todo: wtf with headers? can't remember.
			return -1;
		}
		return 0;
	}
	private int createAuthKey_DigestMsg1(Bytes response)
	{
		//*** resPQ#05162463 nonce:int128 server_nonce:int128 pq:string server_public_key_fingerprints:Vector long = ResPQ
		ITLStream istream = new ITLStream(response);

		if(!istream.matchBytes(SCHEMA.RES_PQ))
			return HLK.STATUS_CODE.HEADER_MISMATCH;

		if(!istream.matchBytes(iclk.nonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;
		
		iclk.setServerNonce(istream.nextInt128());
		
		byte[] pq = istream.nextString();
		if(iclk.setPq(pq)<0)
			return HLK.STATUS_CODE.UNEXPECTED_RESPONSE;

		if(!istream.matchBytes(SCHEMA.VECTOR_LONG))
			return HLK.STATUS_CODE.HEADER_MISMATCH;
		/* COVER THIS:
		Special constructors are not required for Vector int, Vector User, Vector Object, etc.
		— the same universal constructor can be used everywhere:
		vector#1cb5c415 {t:Type} # [ t ] = Vector t;
		 */
		
		int componentsNumber = istream.nextInt32();
		if(componentsNumber<1)
			return HLK.STATUS_CODE.HEADER_MISMATCH;
		
		for(int i=0;i<componentsNumber;i++)
			iclk.addServerFingerPrint(istream.nextNBytes(8));
		//wtf with the RSA key
		//security check about pq and fingerp
		//todo: Man in the middle weak point
		
		return 0;
	}
	private int createAuthKey_SendMsg2(Bytes response)
	{
		response.clear();
		Bytes rsaEncrypted;
		//<editor-fold desc="RSA Encryption">
		//*** p_q_inner_data#83c95aec pq:string p:string q:string nonce:int128 server_nonce:int128 new_nonce:int256 = P_Q_inner_data
		OTLStream toEncMsg = new OTLStream();
		
		toEncMsg.appConr(SCHEMA.P_Q_INNER_DATA);
		
		toEncMsg.appString(iclk.PQ.toByteArray());
		toEncMsg.appString(iclk.P.toByteArray());
		toEncMsg.appString(iclk.Q.toByteArray());

		toEncMsg.appInt128(iclk.nonce);
		toEncMsg.appInt128(iclk.serverNonce);
		iclk.genNewNonce();
		toEncMsg.appBytes(iclk.newNonce);
		
		Bytes dataWithHash = new Bytes();
		dataWithHash.add(ICLK.SHA1(toEncMsg.getPbytes()));
		dataWithHash.add(toEncMsg.getPbytes());
		//commented the nwxt line becuase it does automatically
		dataWithHash.add(iclk.randomBytes(255 - dataWithHash.size())); //set size to 255
		try
		{
			rsaEncrypted = new Bytes(iclk.enRSA(dataWithHash.toPbytes()));
		}
		catch (Exception e)
		{
			return HLK.STATUS_CODE.ENCRYPTION_FAIL;
		}
		//</editor-fold>

		//*** req_DH_params#d712e4be nonce:int128 server_nonce:int128 p:string q:string public_key_fingerprint:long encrypted_data:string = Server_DH_Params

        OTLStream message = new OTLStream();
		
		message.appConr(SCHEMA.REQ_DH_PARAMS);
		
		message.appInt128(iclk.nonce);
		message.appInt128(iclk.serverNonce);
		
		message.appString(iclk.P.toByteArray()); // Big Endian
		message.appString(iclk.Q.toByteArray()); // Big Endian

		message.appBytes(iclk.getServerFingerPrint());

		message.appString(rsaEncrypted.toPbytes());
		
		try
		{
			response.addAll(llk.sendUnencryptedMessage(message.getBytes()));
		}
		catch (IOException e)
		{
			if(e instanceof SocketTimeoutException)
				return HLK.STATUS_CODE.TIMEOUT;
			if(e instanceof InvalidObjectException)
				return HLK.STATUS_CODE.HEADER_MISMATCH;
			return -1;
		}

		return 0;
	}
	private int createAuthKey_DigestMsg2(Bytes response)
	{
//		hlk.log("To Digest Now:\n{");
//		hlk.logHex(response);
//		hlk.log("}");

		//*** server_DH_params_ok#d0e8075c nonce:int128 server_nonce:int128 encrypted_answer:string = Server_DH_Params;

		ITLStream istream = new ITLStream(response);

		if(!istream.matchBytes(SCHEMA.SERVER_DH_PARAMS_OK))
			return HLK.STATUS_CODE.HEADER_MISMATCH;

		if(!istream.matchBytes(iclk.nonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;

		if(!istream.matchBytes(iclk.serverNonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;

		byte[] encryptedAnswer = istream.nextString(); //skipped 4 length bytes

		byte[] decryptedAnswer;
		try
		{
			decryptedAnswer = iclk.decryptTempAES(encryptedAnswer);
		}
		catch (SecurityException e)
		{
			return HLK.STATUS_CODE.SECURITY_RISK;
		}
		catch (Exception e)
		{
			hlk.log(e.toString());
			return HLK.STATUS_CODE.UNKNOWN_FAIL;
		}

//		hlk.log("Decrypted: ");
//		hlk.log(Bytes.fromPbytes(decryptedAnswer).toString());

		//*** server_DH_inner_data#b5890dba nonce:int128 server_nonce:int128 g:int dh_prime:string g_a:string server_time:int = Server_DH_inner_data;

		istream = new ITLStream(Bytes.fromPbytes(decryptedAnswer));

		if(!istream.matchBytes(SCHEMA.SERVER_DH_INNER_DATA))
			return HLK.STATUS_CODE.HEADER_MISMATCH;

		if(!istream.matchBytes(iclk.nonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;

		if(!istream.matchBytes(iclk.serverNonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;

		iclk.setG(istream.nextInt32());

		iclk.setDhPrime(istream.nextString());

		iclk.setG_a(istream.nextString());

		//todo: server time / correction if nessecary

		boolean verif = iclk.verify_g_and_p();
		if(!verif)
			return HLK.STATUS_CODE.SECURITY_RISK;

		return 0;
	}
	private int createAuthKey_SendMsg3(Bytes response)
	{
		response.clear();
		byte[] encrypted; //todo: Re-Used iv string for encryption. Security?
		//<editor-fold desc="AES IGE Encryption">
		OTLStream toEncMsg = new OTLStream();
		//*** client_DH_inner_data#6643b654 nonce:int128 server_nonce:int128 retry_id:long g_b:string = Client_DH_Inner_Data
		toEncMsg.appConr(SCHEMA.CLIENT_DH_INNER_DATA);
		toEncMsg.appInt128(iclk.nonce);
		toEncMsg.appInt128(iclk.serverNonce);
		toEncMsg.appInt64(0); //todo: retry id
		iclk.genG_b();
		toEncMsg.appString(iclk.g_b);

		byte[] sha1InnerData = ICLK.SHA1(toEncMsg.getPbytes());
		Bytes dataWithHash = new Bytes(sha1InnerData);
		dataWithHash.addAll(toEncMsg.getBytes());
		dataWithHash.add(iclk.randomBytes(16 - dataWithHash.size()%16));

		encrypted = iclk.encryptTempAES(dataWithHash.toPbytes());
		//</editor-fold desc="AES IGE Encryption">

		//*** set_client_DH_params#f5045f1f nonce:int128 server_nonce:int128 encrypted_data:string = Set_client_DH_params_answer;

		OTLStream message = new OTLStream();

		message.appConr(SCHEMA.SET_CLIENT_DH_PARAMS);
		message.appInt128(iclk.nonce);
		message.appInt128(iclk.serverNonce);
		message.appString(encrypted);

		try
		{
			response.addAll(llk.sendUnencryptedMessage(message.getBytes()));
		}
		catch (IOException e)
		{
			if(e instanceof SocketTimeoutException)
				return HLK.STATUS_CODE.TIMEOUT;
			if(e instanceof InvalidObjectException)
				return HLK.STATUS_CODE.HEADER_MISMATCH;
			return -1;
		}

		return 0;
	}
	private int createAuthKey_DigestMsg3(Bytes response)
	{
		//*** dh_gen_ok#3bcbf734 nonce:int128 server_nonce:int128 new_nonce_hash1:int128 = Set_client_DH_params_answer;

		ITLStream istream = new ITLStream(response);

		if(!istream.matchBytes(SCHEMA.DH_GEN_OK))
			return HLK.STATUS_CODE.HEADER_MISMATCH;

		if(!istream.matchBytes(iclk.nonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;

		if(!istream.matchBytes(iclk.serverNonce))
			return HLK.STATUS_CODE.DAMAGED_MESSAGE;

		iclk.genAuthKey();

		Bytes inputSha1 = new Bytes(iclk.newNonce);
		inputSha1.add((byte)0x01);
		byte[] auth_key_aux_hash = Bytes.fromPbytes(ICLK.SHA1(iclk.authKey)).subSet(0, 8);
		inputSha1.add(auth_key_aux_hash);

		byte[] expectedSha1 = ICLK.SHA1(inputSha1.toPbytes());
		expectedSha1 = Bytes.fromPbytes(expectedSha1).subSet(4, 20); //128 lower bits
		if(!istream.matchBytes(expectedSha1))
			return HLK.STATUS_CODE.SECURITY_RISK;

		return 0; //Auth Done!!!
	}
	public byte[] getAuthKey()
	{
		return iclk.authKey;
	}
}
