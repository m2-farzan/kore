package kore;
import java.math.BigInteger;
import static kore.ByteTools.*;

/**
 * Created by mostafa on 7/15/17.
 */
public class OTLStream
{
    private Bytes stream;

    public OTLStream()
    {
        stream = new Bytes();
    }

    public Bytes getBytes()
    {
        return stream;
    }
    public byte[] getPbytes()
    {
        return stream.toPbytes();
    }

    public void appConr(byte[] bytes)
    {
        stream.add(bytes);
    }

    public void appInt32(byte[] bytes)
    {
        stream.add(rightPadding(bytes, 4));
    }
    public void appInt64(byte[] bytes)
    {
        stream.add(rightPadding(bytes, 8));
    }
    public void appInt128(byte[] bytes)
    {
        stream.add(leftPadding(bytes, 16)); // Big Endian
    }
    public void appInt32(int a)
    {
        stream.add(littleEndian(a, 4));
    }
    public void appInt64(long a)
    {
        stream.add(littleEndian(a, 8));
    }
    public void appInt128(BigInteger a)
    {
        stream.add(bigEndian(a, 16)); // Big Endian
    }
    public void appBytes(byte[] bytes) { stream.add(bytes); }
    public void appBytes(Bytes bytes) { stream.addAll(bytes); }

    public void appString(byte[] bytes)
    {
        int len = bytes.length;
        if (len <= 253)
        {
            stream.add(littleEndian(len, 1));
            stream.add(bytes);
            stream.add(zeroes( 3 - len%4 ));
        }
        else
        {
            stream.add((byte)0xfe);
            stream.add(littleEndian(len, 3));
            stream.add(bytes);
            stream.add(zeroes( len%4==0? 0: 4-(len)%4 ));
        }
    }
}
