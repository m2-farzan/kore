## Progress Plans for Kore Project

* ~~Structural Design~~

* ~~TCP Line Imp.~~

* Authorization Phase and Plain Encryption **(WIP)**

	* ~~Request 1~~
	* ~~Digesting Answer 1~~
	* ~~Proof of Work~~
	* ~~SHA-1~~
	* ~~RSA~~
	* ~~Request 2~~
	* ~~Digesting Answer 2~~
	* ~~*Mathematics*~~ *(Security Checks To Do)*
	* ~~Request 3~~
	* ~~Digesting Answer 3~~
	* {
	    
        *REFINEMENTS:*
	    * ~~CLK to ICLK~~, RCLK.
        * File Interface
        * ~~IOStream~~
	* }
	* Failure Behaviour
	* Stroring Keys
	
* Simple Messages and Advanced Encryption

	* Send Tg Messages
		* AES + RSA stuff....
	* Read Tg Message at
	
* Vector Queries

	* Read Users List
	* Read Tg Messages Range

* Database and Storage

* Multimedia Messages

* Failure Cases

* Security Check

* Optimization

	* Caching
	* Server Selection