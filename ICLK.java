package kore;

import java.math.BigInteger;
import java.security.*;
import javax.crypto.*;
import java.security.interfaces.*;
import java.security.spec.*;
import java.util.ArrayList;
import java.util.Arrays;
import static kore.ByteTools.*;


public class ICLK
{
	private SecureRandom secureRandom;
	private Cipher cipher;

	public ICLK()
	{
		secureRandom = new SecureRandom();
	}
	
	
	public byte[] nonce;
	
	public void genNonce()
	{
		nonce = new byte[16];
		secureRandom.nextBytes(nonce);
	}
	
	public byte[] newNonce;
	
	public void genNewNonce()
	{
		newNonce = new byte[32];
		secureRandom.nextBytes(newNonce);
	}
	
	public byte[] serverNonce;
	
	public void setServerNonce(byte[] _serverNonce)
	{
		serverNonce = _serverNonce;
	}
	
	public byte[] pq;
	public byte[] p,q;
	public BigInteger PQ, P, Q;
	
	public int setPq(byte[] _pq)
	{
		pq = _pq;
		PQ = new BigInteger(pq);
		int resultCode = factorPq();
		return resultCode;
	}
	
	public int factorPq()
	{
		long theSmallFactor = NativeAgent.semiprimeFactor(pq);
		P = BigInteger.valueOf(theSmallFactor);
		p = bigEndian(P, 4);
		Q = PQ.divide(P);
		q = bigEndian(Q, 4);
		return 0;
	}
	
	public ArrayList<byte[]> serverFingerPrint; //todo: wtf is this thing
	public void addServerFingerPrint(byte[] bytes)
	{
		if(serverFingerPrint==null)
			serverFingerPrint = new ArrayList<byte[]>();
		serverFingerPrint.add(bytes);
	}
	public byte[] getServerFingerPrint()
	{
		return serverFingerPrint.get(0);
	}

	public static byte[] SHA1(byte[] data)
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			return md.digest(data);
		}
		catch (NoSuchAlgorithmException e)
		{
			return null;
		}
	}
	
	public byte[] enRSA(byte[] data) throws Exception
	{
		//todo: PADDING STUFF
		cipherReady();
		return cipher.doFinal(data);
	}


	private byte[] getTmp_aes_key()
	{
		Bytes tmp_aes_key = new Bytes();
		Bytes p1 = new Bytes();
		p1.add(newNonce);
		p1.add(serverNonce);
		Bytes p2 = new Bytes();
		p2.add(serverNonce);
		p2.add(newNonce);
		byte[] P1 = ICLK.SHA1(p1.toPbytes());
		byte[] P2 = Bytes.fromPbytes(ICLK.SHA1(p2.toPbytes())).subSet(0, 12);
		tmp_aes_key.add(P1);
		tmp_aes_key.add(P2);
		return tmp_aes_key.toPbytes();
	}
	private byte[] getTmp_aes_iv()
	{
		Bytes tmp_aes_iv = new Bytes();
		Bytes p1 = new Bytes();
		p1.add(serverNonce);
		p1.add(newNonce);
		Bytes p2 = new Bytes();
		p2.add(newNonce);
		p2.add(newNonce);
		Bytes p3 = Bytes.fromPbytes(newNonce);
		byte[] Q1 = Bytes.fromPbytes(ICLK.SHA1(p1.toPbytes())).subSet(12, 20);
		byte[] Q2 = ICLK.SHA1(p2.toPbytes());
		byte[] Q3 = p3.subSet(0,4);
		tmp_aes_iv.add(Q1);
		tmp_aes_iv.add(Q2);
		tmp_aes_iv.add(Q3);
		return tmp_aes_iv.toPbytes();
	}
	public byte[] decryptTempAES(byte[] data) throws Exception
	{
		byte[] tmp_aes_key = getTmp_aes_key();
		byte[] tmp_aes_iv = getTmp_aes_iv();

		AESIGECipher aesigeCipher = new AESIGECipher(tmp_aes_key,
				tmp_aes_iv);

		Bytes decrypted = Bytes.fromPbytes( aesigeCipher.decrypt(data) );
		byte[] message = decrypted.subSet(20, decrypted.size() - 8); //Hardcoded 8. (The number of Padding Bytes on Rec2)
		byte[] exceptedSha1 = SHA1(message);
		byte[] gottenSha1 = decrypted.subSet(0, 20);
		if(!Arrays.equals(gottenSha1, exceptedSha1))
			throw new SecurityException("SHA-1 of message does not match plain text.");

		return message;
	}
	public byte[] encryptTempAES(byte[] data)
	{
		byte[] tmp_aes_key = getTmp_aes_key();
		byte[] tmp_aes_iv = getTmp_aes_iv();

		AESIGECipher aesigeCipher = new AESIGECipher(tmp_aes_key,
				tmp_aes_iv);

		byte[] encrypted = aesigeCipher.encrypt(data);
		return encrypted;
	}
	
	private void cipherReady() throws Exception
	{
		if(cipher!=null)
			return;
		final BigInteger MODULUS = new BigInteger(Bytes.fromHexString("00C150023E2F70DB7985DED064759"+
				"CFECF0AF328E69A41DAF4D6F01B538135A6F91F8F8B2A0EC9BA9720CE352EFCF6C5680FFC424BD634864"+
				"902DE0B4BD6D49F4E580230E3AE97D95C8B19442B3C0A10D8F5633FECEDD6926A7F6DAB0DDB7D457F9EA"+
				"81B8465FCD6FFFEED114011DF91C059CAEDAF97625F6C96ECC74725556934EF781D866B34F011FCE4D83"+
				"5A090196E9A5F0E4449AF7EB697DDB9076494CA5F81104A305B6DD27665722C46B60E5DF680FB16B2106"+
				"07EF217652E60236C255F6A28315F4083A96791D7214BF64C1DF4FD0DB1944FB26A2A57031B32EEE64AD"+
				"15A8BA68885CDE74A5BFC920F6ABF59BA5C75506373E7130F9042DA922179251F").toPbytes());
		final BigInteger PUBLIC_EXPONENT = BigInteger.valueOf(65537);
		RSAPublicKeySpec keySpec = new RSAPublicKeySpec(MODULUS, PUBLIC_EXPONENT);
		RSAPublicKey publicKey = (RSAPublicKey)KeyFactory.getInstance("RSA").generatePublic(keySpec);
		cipher = Cipher.getInstance("RSA/ECB/NoPadding"); //todo: T'gram instructed to use manual padding (no pks1). Security issues?
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
	}
	private void cipherReset() throws Exception
	{
		cipher = null;
		cipherReady();
	}
	
	public byte[] randomBytes(int n)
	{
		byte[] r = new byte[n];
		secureRandom.nextBytes(r);
		return r;
	}

	private int g;
	public void setG(int _g) { g = _g; }
	private byte[] dhPrime;
	public void setDhPrime(byte[] _dhPrime) { dhPrime = _dhPrime; }
	private byte[] g_a;
	public void setG_a(byte[] _g_a) { g_a = _g_a; }
	private byte[] b;
	private void genB()
	{ b = randomBytes(256); }
	public byte[] g_b;
	public void genG_b()
	{
		genB();
		BigInteger G = BigInteger.valueOf(g);
		BigInteger P = new BigInteger(1, dhPrime);
		BigInteger B = new BigInteger(1, b);
		BigInteger G_B = G.modPow(B, P);
		g_b = unsignedBigIntBigEnd(G_B);
	}

	public boolean verify_g_and_p()
	{
		//todo: Cryptography verification of g, dh_prime(p). Maybe use a static table.
		return true;
	}

	public byte[] authKey;
	public void genAuthKey()
	{
		BigInteger G_A = new BigInteger(1, g_a);
		BigInteger B = new BigInteger(1, b);
		BigInteger P = new BigInteger(1, dhPrime);
		BigInteger G_B = G_A.modPow(B, P);
		authKey = G_B.toByteArray();
		if (authKey[0]==0x00 && authKey.length>256) //because of sign problems
		{
			authKey = new Bytes(authKey).subSet(1, 257);
		}
	}

	public String report() //debug tool
	{
		StringBuilder r = new StringBuilder();
		try
		{
			r.append("nonce:\n");
			r.append(toHex(nonce));
			r.append("new nonce:\n");
			r.append(toHex(newNonce));
			r.append("server nonce:\n");
			r.append(toHex(serverNonce));
			r.append("pq:\n");
			r.append(toHex(pq));
			r.append("p:\n");
			r.append(toHex(p));
			r.append("q:\n");
			r.append(toHex(q));
			r.append("g:\n");
			r.append(g);
			r.append("\ndhprime:\n");
			r.append(toHex(dhPrime));
			r.append("g_a:\n");
			r.append(toHex(g_a));
			r.append("b:\n");
			r.append(toHex(b));
			r.append("g_b:\n");
			r.append(toHex(g_b));
			r.append("authkey:\n");
			r.append(toHex(authKey));
		}
		catch (NullPointerException e)
		{
			r.append("Null field found. ICLK object is incomplete.");
		}
		return r.toString();
	}
}
