package kore;

/**
 * Created by mostafa on 7/15/17.
 */
public interface FileInterface
{
    //For save/loading small data, eg. Auth Key, flags, etc.
    int saveBytes(byte[] bytes, String key);
    int loadBytes(byte[] bytes, String key);
    boolean exists(String key);
}
