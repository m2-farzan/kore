package kore;
import java.util.Arrays;

import static kore.ByteTools.*;

/**
 * Created by mostafa on 7/15/17.
 */
public class ITLStream
{
    Bytes stream;
    int pos = 0;

    public ITLStream(byte[] bytes)
    {
        stream = new Bytes(bytes);
    }
    public ITLStream(Bytes bytes)
    {
        stream = bytes;
    }

    public void rewind()
    {
        pos = 0;
    }
    public void fseek(int _pos)
    {
        pos = _pos;
    }
    public int getLen()
    {
        return stream.size();
    }
    public Bytes getBytes()
    {
        return stream;
    }
    public boolean over()
    {
        return pos >= stream.size();
    }

    public int nextInt32()
    {
        pos += 4;
        return inverseLittleEndian(stream.subSet(pos - 4, pos));
    }
    public long nextInt64()
    {
        pos += 8;
        return inverseLittleEndian(stream.subSet(pos - 8, pos));
    }
    public byte[] nextInt128()
    {
        pos += 16;
        return stream.subSet(pos - 16, pos);
    }
    public byte[] nextString()
    {
        if (stream.get(pos) != (byte)0xfe)
        {
            int len = Math.abs((int)stream.get(pos));
            int opos = pos + 1;
            pos += len + 4 - len%4;
            return stream.subSet(opos, opos + len);
        }
        else
        {
            int len = inverseLittleEndian(stream.subSet(pos+1, pos+4));
            int opos = pos + 4;
            pos = opos + len; //god! wrong pos += opos + len; took half a day to show up
            pos += len%4 == 0 ? 0 : 4 - len%4;
            return stream.subSet(opos, opos + len);
        }
    }
    public byte[] nextConr()
    {
        pos += 4;
        return stream.subSet(pos - 4, pos);
    }
    public byte[] nextNBytes(int n)
    {
        pos += n;
        return stream.subSet(pos - n, pos);
    }
    public boolean matchBytes(byte[] bytes)
    {
        pos += bytes.length;
        return Arrays.equals(bytes, stream.subSet(pos - bytes.length, pos));
    }
}
