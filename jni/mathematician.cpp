#include <jni.h>
#include <math.h>

unsigned long long semiprimeFactor(unsigned long long N)
{
	unsigned long long n = sqrt(N) + 1;
	unsigned long long j = 1;
	unsigned long long djs;
	for(;;)
	{
		djs = n * n - N;
		j = sqrt(djs);
		if(j * j == djs)
		{
			return n - j;
		}
		n++;
	}
}
bool isLittleEndianMachine()
{
    int n = 1;
    return (*(char *)&n == 1);
}
unsigned long long bigEndianToULL(jbyte* p)
{
    unsigned long long r = *((unsigned long long*)p);
    if(!isLittleEndianMachine())
        return r;
    r =
     ((r&0x00000000000000FFull)<<8*7)|((r&0x000000000000FF00ull)<<8*5)
    |((r&0x0000000000FF0000ull)<<8*3)|((r&0x00000000FF000000ull)<<8*1)
    |((r&0x000000FF00000000ull)>>8*1)|((r&0x0000FF0000000000ull)>>8*3)
    |((r&0x00FF000000000000ull)>>8*5)|((r&0xFF00000000000000ull)>>8*7);

    return r;
}

extern "C" JNIEXPORT jlong JNICALL Java_kore_NativeAgent_SEMIPRIMEFACTOR(JNIEnv* env, jobject o, jbyteArray data)
{
    jboolean isCopy;
    jbyte* semiprimeBytes = env->GetByteArrayElements(data, &isCopy);

    unsigned long long pq = bigEndianToULL(semiprimeBytes);
    jlong r = (jlong)semiprimeFactor(pq);

    env->ReleaseByteArrayElements(data, semiprimeBytes, JNI_ABORT);
    return r;
}