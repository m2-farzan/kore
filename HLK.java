package kore;

public class HLK
{
	//<editor-fold desc="Constants">
	public static class REQUEST_CODE
	{
		public final static int  DEBUG = -1;
		public final static int  UNKNOWN_REQUEST = 0;
		public final static int  AUTH_KEY_GEN = 1;
		public final static int  AUTH_KEY_LOAD = 7;
		public final static int  SEND_MESSAGE = 2;
		public final static int  GET_MESSAGE_AT = 3;
		public final static int  GET_MESSAGES_AT = 4;
		public final static int  GET_ALL_MESSAGES = 5;
		public final static int  TEST_STRING = 6;
	}
	public static class STATUS_CODE
	{
		public final static int  SUCCESS = 	0;
		public final static int  UNKNOWN_FAIL =	-1;
		public final static int  CONTINUED = 	1;
		public final static int  TIMEOUT = 	-2;
		public final static int  INTERRUPTED = -3;
		public final static int  BUSY = -4;
		public final static int  UNEXPECTED_RESPONSE = -5;
		public final static int  HEADER_MISMATCH = -6;
		public final static int  DAMAGED_MESSAGE = -7;
		public final static int	 ENCRYPTION_FAIL = -8;
		public final static int	 SECURITY_RISK = -9;
	}
	//</editor-fold>
	
	//<editor-fold desc="Debug Functions"
	public void testString()
	{
		String randomString;
//		randomString = Integer.toHexString((new Random()).nextInt(128)).toUpperCase();
		randomString = Long.toString(System.nanoTime());
		TestString runnable = new TestString(randomString);
		startThread(runnable);
	}
	private class TestString implements Runnable
	{
		String string;
		public TestString(String _string)
		{
			string = _string;
		}
		public void run()
		{
			try
			{
				postTestString();
			}
			catch (InterruptedException e)
			{
				postGeneralFailure(STATUS_CODE.INTERRUPTED);
			}
		}
		private void postTestString() throws InterruptedException
		{
			Thread.sleep(1000);
			responser.postMessage(REQUEST_CODE.TEST_STRING, STATUS_CODE.SUCCESS, "test");
		}
	}
	public void log(String text)
	{
		responser.log(text);
	}
	public void logHex(Bytes bytes)
	{
		responser.log(bytes.toString());
	}
	//</editor-fold>
	
	
	private Thread thread;
	private ResponseInterface responser;
	private FileInterface filer;
	private MLK mlk;
	
	
	public HLK(ResponseInterface _responser, FileInterface _filer)
	{
		responser = _responser;
		filer = _filer;
		responser.log("HLK is initialising...");
		thread = new Thread();
		setupKores();
		responser.log("HLK is running.");
	}
	private void setupKores()
	{
		mlk = new MLK(this);
	}
	
	
	private void startThread(Runnable runnable)
	{
		if(isThreadBusy())
			return;
		//todo: calling stopThread for safety??
		//cause it seems that isThreadBusy isn't precise and sometimes returns false when
		//thread is running; at least when it is on a sleep mode.
		thread = new Thread(runnable);
		thread.start();
	}
	private boolean isThreadBusy()
	{
		if(thread.isAlive())
		{
			postGeneralFailure(STATUS_CODE.BUSY);
			return true;
		}
		return false;
	}
	public void stopThread()
	{
		if(!thread.isAlive())
			return;
		thread.interrupt(); //Todo: what if interrupt isn't enough? More force?
	}
	public void nap()
	{
		final int NAP_MILLIS=1000;
		try{
			Thread.sleep(NAP_MILLIS);
		}
		catch (Exception e)
		{}
	}
	public void forceStop()
	{
		stopThread();
	}

	private void postGeneralFailure(int failCode) // not useful. use single line resp.postErr
	{
		if(failCode>=0)
			return;
		responser.postError(failCode);
	}

	public void createAuthKey()
	{
		createAuthKey("default");
	}
	public void createAuthKey(String keyId)
	{
		CreateAuthKey runnable = new CreateAuthKey(keyId);
		startThread(runnable);
	}
	private class CreateAuthKey implements Runnable
	{
		String keyId;
		public CreateAuthKey(String _keyId)
		{
			keyId = _keyId;
		}
		public void run()
		{
			log("creating auth key...");
			int result = mlk.createAuthKey();
			if(result!=0) //STATUS_CODE.SUCCESS
			{
				mlk.closeAllPorts();
				postGeneralFailure(result);
				return;
			}
			log("auth key creation successful.\nsaving auth key...");
			result = filer.saveBytes(mlk.getAuthKey(), "authkey_"+keyId);
			if(result!=0)
			{
				postGeneralFailure(result);
				return;
			}
			responser.postMessage(REQUEST_CODE.AUTH_KEY_GEN, 0, null);
			log("auth key save successful.");
		}
	}

	public void loadAuthKey()
	{
		loadAuthKey("default");
	}
	public void loadAuthKey(String keyId)
	{
		LoadAuthKey runnable = new LoadAuthKey(keyId);
		startThread(runnable);
	}
	private class LoadAuthKey implements Runnable
	{
		String keyId;
		public LoadAuthKey(String _keyId)
		{
			keyId = _keyId;
		}
		public void run()
		{
			log("loading auth key...");
			byte[] bytes = new byte[256]; //auth key is 256 bytes long
			int result = filer.loadBytes(bytes, "authkey_"+keyId);
			if(result!=0)
			{
				postGeneralFailure(result);
				return;
			}
			responser.postMessage(REQUEST_CODE.AUTH_KEY_LOAD, 0, null);
			log("auth key loaded successfully.");
		}
	}
}
