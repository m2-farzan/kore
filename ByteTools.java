package kore;

import java.math.BigInteger;
import java.nio.*;
import java.util.Arrays;

public class ByteTools
{
	public static byte[] littleEndian(int x, int byteSize)
	{
		return littleEndian((long)x, byteSize);
	}
	public static byte[] littleEndian(long x, int byteSize)
	{
		if(byteSize<8)
		{
			//This is due to the problem that ByteBuffer.putLong can't
			//write on a byte[] of length <8.
			Bytes extraSpaced = new Bytes(littleEndian(x, 8));
			//There are extra zeroes at right
			//We only return the leftmost bytes
			return extraSpaced.subSet(0, byteSize);
		}
		ByteBuffer byteBuffer = ByteBuffer.allocate(byteSize);
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		byteBuffer.putLong(x);
		return byteBuffer.array();
	}
	public static byte[] littleEndian(BigInteger x, int byteSize)
	{
		byte[] r = x.toByteArray();
		byte[] reversed = reverse(r);
		return rightPadding(reversed, byteSize);
	}
	public static byte[] bigEndian(int x, int byteSize)
	{
		return bigEndian((long)x, byteSize);
	}
	public static byte[] bigEndian(long x, int byteSize)
	{
		return reverse(littleEndian(x, byteSize));
	}
	public static byte[] bigEndian(BigInteger x, int byteSize)
	{
		byte[] r = x.toByteArray();
		return leftPadding(r, byteSize);
	}
	
	public static int inverseBigEndian(byte[] bytes)
	{
		ByteBuffer byteBuffer = ByteBuffer.wrap(leftPadding(bytes,4));
		return byteBuffer.getInt();
	}
	public static int inverseLittleEndian(byte[] bytes)
	{
		ByteBuffer byteBuffer = ByteBuffer.wrap(reverse(rightPadding(bytes,4)));
		return byteBuffer.getInt();
	}
	public static long inverseBigEndian64(byte[] bytes)
	{
		ByteBuffer byteBuffer = ByteBuffer.wrap(leftPadding(bytes,8));
		return byteBuffer.getLong();
	}
	public static long inverseLittleEndian64(byte[] bytes)
	{
		ByteBuffer byteBuffer = ByteBuffer.wrap(reverse(rightPadding(bytes,8)));
		return byteBuffer.getLong();
	}
	
	public static byte[] schemaBytes(int x)
	{
		return littleEndian(x, 4);
	}
	public static byte[] reverse(byte[] a)
	{
		int x = a.length;
		byte[] t = a.clone();
		byte[] r = new byte[x];
		for(int i=0;i<x;i++)
			r[i]=a[x-i-1];
		return r;
	}
	public static byte[] leftPadding(byte[] bytes, int desiredLength)
	{
		int length = bytes.length;
		int diff = desiredLength-length;
		if(diff<=0)
			return bytes;
		byte[] padding = new byte[diff];
		Arrays.fill(padding,(byte)0);
		return combine(padding, bytes);
	}
	public static byte[] rightPadding(byte[] bytes, int desiredLength)
	{
		int length = bytes.length;
		int diff = desiredLength-length;
		if(diff<=0)
			return bytes;
		boolean JUST_FOR_KILLING_DUPLICATE_CODE_WARNING;
		byte[] padding = new byte[diff];
		Arrays.fill(padding,(byte)0);
		return combine(bytes, padding);
	}
	public static byte[] combine(byte[] a, byte[] b)
	{
		int aLen = a.length;
		int bLen = b.length;
		int rLen = aLen + bLen;
		byte r[] = new byte[rLen];
		for(int i=0;i<aLen;i++)
			r[i]=a[i];
		for(int i=aLen;i<rLen;i++)
			r[i]=b[i-aLen];
		return r;
	}
	public static boolean isAllZero(byte[] x)
	{
		int length = x.length;
		for(int i=0;i<x.length;i++)
			if(x[i]!=(byte)0x00)
				return false;
		return true;
	}
	public static byte[] zeroes(int n)
	{
		byte[] r = new byte[n];
		Arrays.fill(r,(byte)0);
		return r;
	}
	public static byte[] firstByteDenotesLengthUnpack(byte[] bytes)
	{
		int a = (int)bytes[0];
		byte[] r = new byte[a];
		for(int i=0;i<a;i++)
			r[i] = bytes[i+1];
		return r;
	}
	public static byte[] firstByteDenotesLengthPack(int x, int byteSize)
	{
		return firstByteDenotesLengthPack((long)x, byteSize);
	}
	public static byte[] firstByteDenotesLengthPack(long x, int finalByteSize)
	{
		int numberByteSize = finalByteSize - 1;
		byte[] r = littleEndian(x, numberByteSize);
		int nUsedBytes = numberByteSize;
		for(int i=numberByteSize-1;i>=0;i--)
			if((int)r[i]==0)
				nUsedBytes--;
			else
				break;
		Bytes R = new Bytes();
		R.add((byte)nUsedBytes);
		R.add(r);
		return R.toPbytes();
	}
	public static byte[] firstByteDenotesLengthPack(BigInteger x, int finalByteSize)
	{
		int numberByteSize = finalByteSize - 1;
		byte[] r = littleEndian(x, numberByteSize);
		int nUsedBytes = numberByteSize;
		for(int i=numberByteSize-1;i>=0;i--)
			if((int)r[i]==0)
				nUsedBytes--;
			else
				break;
		Bytes R = new Bytes();
		R.add((byte)nUsedBytes);
		R.add(r);
		return R.toPbytes();
	}
	public static byte[] firstByteDenotesLengthPack_BE(long x, int finalByteSize)
	{
		byte[] r = firstByteDenotesLengthPack(x, finalByteSize);
		int n = (int)r[0];
		for(int i=0; i<n/2; i++)
		{
			//swap r(i+1), r(n-i)
			byte t = r[i+1];
			r[i+1] = r[n-i];
			r[n-i] = t;
		}
		return r;
	}
	public static byte[] firstByteDenotesLengthPack_BE(BigInteger x, int finalByteSize)
	{
		byte[] r = firstByteDenotesLengthPack(x, finalByteSize);
		int n = (int)r[0];
		for(int i=0; i<n/2; i++)
		{
			//swap r(i+1), r(n-i)
			byte t = r[i+1];
			r[i+1] = r[n-i];
			r[n-i] = t;
		}
		return r;
	}

	public static byte[] unsignedBigIntBigEnd(BigInteger n)
	{
		byte[] bytes = n.toByteArray();

		if(bytes[0] != 0x00)
			return bytes;

		byte[] bytex = new byte[bytes.length-1];
		for(int i=1; i<bytes.length; i++)
			bytex[i-1] = bytes[i];
		return bytex;
	}
	public static byte[] fromHex(String hex)
	{
		char[] raw = hex.toCharArray();
		int rawLen = raw.length;
		char[] str = new char[rawLen];

		int n=0;
		for(int i=0; i<rawLen; i++)
		{
			if(raw[i]==' '||raw[i]=='\n')
				continue;
			str[n] = raw[i];
			n++;
		}
		//removed escape chars

		byte[] bytes = new byte[n/2];
		String s;
		for(int i=0; i<n; i+=2)
		{
			s = "";
			s += str[i];
			s += str[i+1];
			bytes[i/2] = (byte)(Integer.parseInt(s, 16));
		}
		return bytes;
	}
	public static String toHex(byte[] bytes)
	{
		if(bytes.length==0)
		{
			return ("\n[Empty Bytes]");
		}
		final int colSize = 16;
		int length = bytes.length;
		String r = "";
		for(int i = 0; i < length/colSize + 1; i++)
		{
			if(i!=0)
				r+="\n";
			for(int j=0;j<colSize;j++)
			{
				if(j==colSize/2)
					r+=" ";
				if(i*colSize+j >= length)
					break;
				String p = String.format("%02X ", bytes[i*colSize+j]);
				r+=(p+" ");
			}
		}
		return r;
	}
}
