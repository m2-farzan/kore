## Telegram API Interface (Core Layer)
#### Description
This is a Java library that implements Telegram protocol and schema in order to enable programmers to comminucate with the Telegram servers easily.

This API layer is also called *Core Layer*, which is added to UI Layer and AppLayer, building a complete Telegram-based application. The purpose of this layer is to encapsulate the proccess of Telegram customized protocol serializing, as well as schema building. Addintational features will include database management, file management, etc.

On the code side, the package is always refered as `kore`, so as to avoid potential clash with the system keyword *core*. In documantations, however, both spellings are used.

#### Installation
In order to install and use this library package in your projects, you need to first clone the package folder aside your Java program package (eg. the `/src/` folder).

	cd java-program/src
	git clone git@gitlab.com:m2-farzan/kore.git

Just before your program can use the library, the native library file must be installed. Note that this is very important when the program is to be used on other machines. The native file(s) are included in path `/kore/jni/`. Implementing native code depends on the environment the app is being developed for. For Linux-Java platform, for instance, this commands will add the native library to the OS:

	cd java-program/src/kore/jni
	g++ mathematician.cpp -shared -o libmathematician.so
	sudo mv libmathematician.so /usr/lib/libmathematician.so

And that's it. Now on the Java side, the program can create an instance of the class `HLK`, and the rest is straight ahead.

**Just one point:** One of features implemented in kore package is multithreading; that is, the programmer need not to take care of threading and syncing. However, this comes with a drawback. If a function is to run instantly, while the actual task is not instant, the function cannot return anything directly to its caller. That is, we need to implement another interface to enable functions to return the called values or success/fail result.

In order to do that, we have created the Java Interface `ResponseInterface`. Detailed information of the `ResponseInterface` is avalible in the documentation file. The point here is that, before you can use the package, you need to write a class which implements the `ResponseInterface`. This class is to be fed to the `HLK` on construction.

Briefly, the `ResponseInterface` can be as simple as a `System.out.println` logger, if you are developing for Java line environment, or if it is an Android program, `Handler`s can be used.

#### Credits
Project by Mostafa Farzan (m2-farzan)  
m2_farzan@yahoo.com